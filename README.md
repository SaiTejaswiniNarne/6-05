# 44-563 Group 06-05 - ECommerce App

# About Us
Team Members

>**Customer - Sai Tejaswini**

>**Product - Harika Naidu**

>**Order - Chetty Ragava, Harshitha**

>**Order Line Item - Priyanka Bonam**
*********************
# Includes:
- Customers
- Products
- Orders
- OrderLine

# Uses:
- JSON
- JavaScript
- EJS

# Instructions:
- Run app locally
- node app.js
- From index.js choose an action
- GET a view or GET JSON
- Use available buttons to
- Create, Edit, Delete, or View Details

# Our Repository Link: [https://bitbucket.org/SaiTejaswiniNarne/6-05/src/master/]
# Our Heroku app Link: [https://project-6-05.herokuapp.com/]