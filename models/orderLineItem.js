/**
*@author Priyanka Bonam. S534090
**/

const mongoose = require('mongoose')

const orderLineItemSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  orderID: {
    type: Number,
    required: true,
    default: 00000
  },
  gilineNumberven: {
    type: Number,
    required: false,
    default: 1
  },
  product: {
    type: String,
    required: false,
    default: '0000'
  },
  quantity: {
    type: Number,
    required: false,
    default: 1
}
})
module.exports = mongoose.model('OrderLineItem', orderLineItemSchema)
